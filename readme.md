# Treasure chest

Accessible via [loot-treasure-chest @ herokuapp](https://loot-treasure-chest.herokuapp.com/)

Treasure chest is a small side project I developed with one intent: to deploy a simple application within the span of a weekend. The goals of this exercise are the following:

  - force myself to make compromises to be able to produce a minimum viable project within a short time frame;
  - work on a core idea with limited time for embellishment, while still trying to maintain decent code quality and best practices;
  - focus on the deployment aspect of the project more than the code, which I already feel comfortable with.

The final application allows a user to create a custom piece of loot for tabletop role playing games, and to share it either through a link or a QR code.

In the end, it took me a couple more evenings to deal with troubleshooting but eventually managed to deploy the whole package.

## Backend

TypeScript, TypeORM, express, PostgreSQL. No middleware, no frameworks that wrap express. The interface is very minimal since there are only two endpoints.

## Frontend

TypeScript, React, StyledComponents. No state management library, as there is no relevant global state to handle. Component lifecycle is directly managed through hooks.

## Deployment

Npm on heroku. Navigating through the little details of the cloud environment definitely took a good deal of time, especially when it came to self-signed certificates. Heroku also provisions a simple PostgreSQL instance for free tier applications.

## CI/CD

As there is not enough code to justify extensive testing, BitBucket Pipelines only run to check format and linting errors.