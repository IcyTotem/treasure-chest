import { MigrationInterface, QueryRunner } from 'typeorm';

export class Baseline1597576730818 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE loot(
            id text primary key,
            name text not null,
            description text not null,
            icon text not null,
            encumbrance int,
            value int
        )`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('DROP TABLE loot');
    }
}
