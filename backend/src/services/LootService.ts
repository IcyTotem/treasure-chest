import { Repository } from 'typeorm';
import { Loot } from '@core/entities/Loot';

type LootCreationMeta = Omit<Loot, 'id'>;

const blank = /^\s*$/i;
const maxIconLength = 128;
const maxNameLength = 512;
const maxDescriptionLength = 50000;

const epochBaseline = Date.parse('2020-01-01T00:00:00');

export class LootService {
    public constructor(private readonly lootRepository: Repository<Loot>) {}

    public async create(meta: LootCreationMeta): Promise<Loot> {
        if (blank.test(meta.name)) {
            throw new Error('Name cannot be blank');
        }

        if (meta.name.length > maxNameLength) {
            throw new Error(`Name exceeds maximum length of ${maxNameLength} characters`);
        }

        if (meta.description.length > maxDescriptionLength) {
            throw new Error(`Description exceeds maximum length of ${maxDescriptionLength} characters`);
        }

        if (meta.icon.length > maxIconLength) {
            throw new Error('Invalid icon');
        }

        if (meta.encumbrance && meta.encumbrance < 0) {
            throw new Error('Encumbrance cannot ne negative');
        }

        if (meta.value && meta.value < 0) {
            throw new Error('Value cannot ne negative');
        }

        return this.lootRepository.save({
            ...meta,
            id: this.generateId(),
        });
    }

    public async find(id: string): Promise<Loot> {
        return this.lootRepository.findOne(id);
    }

    private generateId(): string {
        const currentTime = new Date().getTime();
        const randomSalt = Math.floor(Math.random() * 10000);
        const seed = `${currentTime - epochBaseline}:${randomSalt}`;
        const id = Buffer.from(seed, 'utf-8').toString('base64');
        return id;
    }
}
