import { Repository } from 'typeorm';
import { Loot } from './entities/Loot';
import { Database } from './Database';
import { LootService } from './services/LootService';

export class ServiceContainer {
    public async lootRepository(): Promise<Repository<Loot>> {
        return Database.repository(Loot);
    }

    public async lootService(): Promise<LootService> {
        return new LootService(await this.lootRepository());
    }
}

export const serviceContainer = new ServiceContainer();
