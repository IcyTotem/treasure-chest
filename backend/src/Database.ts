import * as path from 'path';
import { createConnection, Connection, getConnection, EntityManager, getManager, Repository } from 'typeorm';

export class Database {
    public static get url(): string {
        // This is also the same variable used by heroku by default
        return process.env['DATABASE_URL'];
    }

    public static get developerMode(): boolean {
        return 'true' === process.env['DEV'];
    }

    static async connection(): Promise<Connection> {
        try {
            return getConnection();
        } catch (e) {
            return createConnection({
                type: 'postgres',
                url: Database.url,
                entities: [path.resolve(__dirname, 'entities/*.ts')],
                // It is of PARAMOUNT importance that this is set to false, otherwise the postgres
                // schema will be WIPED on startup, including all custom constraints and triggers!
                synchronize: false,
                logging: false,
                extra: Database.developerMode
                    ? undefined
                    : {
                          ssl: true,
                          rejectUnauthorized: false, // Certificates provided by heroku are self-signed?
                      },
            });
        }
    }

    static async transaction(callback: (em: EntityManager) => Promise<void>): Promise<void> {
        await getManager().transaction(callback);
    }

    static async repository<Entity>(target: { new (): Entity }): Promise<Repository<Entity>> {
        const connection = await Database.connection();
        return connection.getRepository(target);
    }

    static async entityManager(): Promise<EntityManager> {
        return getManager();
    }
}
