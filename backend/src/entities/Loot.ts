import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class Loot {
    @PrimaryColumn('text')
    id: string;

    @Column('text')
    name: string;

    @Column('text')
    description: string;

    @Column('text')
    icon: string;

    @Column('int')
    encumbrance?: number;

    @Column('int')
    value?: number;
}
