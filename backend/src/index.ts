import * as path from 'path';
import * as fs from 'fs';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { serviceContainer } from './Container';

// PORT is defined by heroku
const PORT = process.env['PORT'] ?? 7113;
const FRONTEND_STATIC_FILES_PATH = process.env['FRONTEND_STATIC_FILES_PATH'];

const app = express();
app.use(bodyParser.json());

const handle = (res: express.Response, e: unknown) => {
    if (e instanceof Error) {
        res.status(400).send({ error: e.message });
    } else {
        res.status(500).send({ error: e.toString() });
    }
};

app.post('/api/loot', async (req, res) => {
    try {
        const lootService = await serviceContainer.lootService();
        const loot = await lootService.create({
            name: req.body.name,
            description: req.body.description,
            icon: req.body.icon,
            encumbrance: req.body.encumbrance ? Number(req.body.encumbrance) : undefined,
            value: req.body.value ? Number(req.body.value) : undefined,
        });

        res.header('Access-Control-Allow-Origin', '*');
        res.json(loot);
    } catch (e) {
        handle(res, e);
    }
});

app.get('/api/loot/:id', async (req, res) => {
    try {
        const lootService = await serviceContainer.lootService();
        const loot = await lootService.find(req.params.id);

        res.header('Access-Control-Allow-Origin', '*');
        res.json(loot);
    } catch (e) {
        handle(res, e);
    }
});

// Serve static files generated for a react production build from the given path
if (fs.existsSync(FRONTEND_STATIC_FILES_PATH)) {
    const absoluteStaticFilesPath = path.resolve(FRONTEND_STATIC_FILES_PATH);
    app.use(express.static(absoluteStaticFilesPath));

    const indexPath = path.resolve(absoluteStaticFilesPath, 'index.html');
    if (!fs.existsSync(indexPath)) {
        console.warn(
            `A path to frontend files was given ("${absoluteStaticFilesPath}"),`,
            'but it contains no index.html.',
            'Did you build the frontend properly?',
        );
    } else {
        app.get('*', (req, res) => res.sendFile(indexPath));
    }
}

app.listen(PORT, () => console.log(`Webapp server listening on port ${PORT}!`));
