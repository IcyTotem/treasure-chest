import React, { useState, useCallback } from 'react';
import styled from 'styled-components';
import QRCode from 'qrcode.react';
import { CreateItemCard, ItemMeta } from './CreateItemCard';
import { createLoot, Loot } from '../api/api';
import { ViewItemCard } from './ViewItemCard';

const CenteredContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const PageContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const CreationBanner = styled.p`
  text-align: center;
`;

const NewLink = styled.a`
  font-family: monospace;
  font-size: 1.2em;
  padding: 10px;
  background-color: #a3e256;
  color: #333;
  border: 1px solid black;
`;

const QRCodeContainer = styled.div`
  padding: 15px;
  background-color: white;
`;

export const CreationPage = () => {
  const [inputDisabled, setInputDisabled] = useState(false);
  const [createdLoot, setCreatedLoot] = useState<Loot>();

  const onCreateAnotherClick = useCallback(() => window.location.reload(), []);
  const onSubmit = useCallback(
    async (itemMeta: ItemMeta) => {
      setInputDisabled(true);

      try {
        const loot = await createLoot(itemMeta);
        setCreatedLoot(loot);
      } catch (e) {
        alert(e.error ?? 'Something went wrong :(');
      } finally {
        setInputDisabled(false);
      }
    },
    [setCreatedLoot, setInputDisabled],
  );

  if (createdLoot) {
    const link = `${window.location.origin}/loot/${createdLoot.id}`;
    return (
      <PageContainer>
        <CenteredContainer>
          <ViewItemCard {...createdLoot} />
        </CenteredContainer>
        <CreationBanner>
          You have created new fantastic loot! Here is a link to it, or a QR code you can download and print if you
          prefer:
        </CreationBanner>
        <CreationBanner>
          <NewLink href={link}>Copy this link!</NewLink>
        </CreationBanner>
        <CenteredContainer style={{ marginTop: 20 }}>
          <QRCodeContainer>
            <QRCode value={link} />
          </QRCodeContainer>
        </CenteredContainer>
        <CenteredContainer style={{ marginTop: 20 }}>
          <button onClick={onCreateAnotherClick}>Create another</button>
        </CenteredContainer>
      </PageContainer>
    );
  }

  return (
    <PageContainer>
      <CenteredContainer>
        <CreateItemCard onSubmit={onSubmit} disabled={inputDisabled} />
      </CenteredContainer>
    </PageContainer>
  );
};
