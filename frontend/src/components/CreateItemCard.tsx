import React, { useState, useCallback, ChangeEvent } from 'react';
import { BaseItemCard } from './BaseItemCard';
import styled from 'styled-components';
import ReactMarkdown from 'react-markdown';
import { RpgIconSelect } from './RpgIconSelect';

const InvisibleNameInput = styled.textarea`
  font-size: 1em;
  text-align: center;
  overflow: hidden;
  width: 100%;
`;

const InvisibleDescriptionInput = styled.textarea`
  font-size: 1em;
  width: 100%;
`;

const InvisibleNumberInput = styled.input`
  font-size: 1em;
  max-width: 75px;
`;

const Meta = styled.span`
  margin-right: 15px;
  & i.ra {
    margin-right: 5px;
  }
`;

const BodyWrapper = styled.div`
  margin-top: 10px;
  margin-bottom: 10px;
`;

const SelectContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-bottom: 10px;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 20px;
`;

export interface ItemMeta {
  name: string;
  description: string;
  icon: string;
  encumbrance?: number;
  value?: number;
}

export interface Props {
  onSubmit: (item: ItemMeta) => void;
  disabled?: boolean;
}

export const CreateItemCard = (props: Props) => {
  const [name, setName] = useState('');
  const onNameChange = useCallback((e: ChangeEvent<HTMLTextAreaElement>) => setName(e.target.value), [setName]);

  const [description, setDescription] = useState('');
  const onDescriptionChange = useCallback((e: ChangeEvent<HTMLTextAreaElement>) => setDescription(e.target.value), [
    setDescription,
  ]);

  const [isPreview, setPreview] = useState(false);
  const onPreviewChange = useCallback((e: ChangeEvent<HTMLInputElement>) => setPreview((v) => !v), [setPreview]);

  const [encumbrance, setEncumbrance] = useState<number>();
  const onEncumbranceChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => setEncumbrance(Number(e.target.value) ?? undefined),
    [setEncumbrance],
  );

  const [value, setValue] = useState<number>();
  const onValueChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => setValue(Number(e.target.value) ?? undefined),
    [setValue],
  );

  const [icon, setIcon] = useState('');
  const onSubmit = () =>
    props.onSubmit({
      name,
      description,
      icon,
      encumbrance,
      value,
    });

  const header = (
    <React.Fragment>
      <SelectContainer>
        <RpgIconSelect onChange={setIcon} disabled={props.disabled} />
      </SelectContainer>
      <InvisibleNameInput
        rows={1}
        value={name}
        onChange={onNameChange}
        placeholder="Item name"
        disabled={props.disabled}
      />
    </React.Fragment>
  );

  const body = (
    <BodyWrapper>
      {isPreview ? (
        <div style={{ minHeight: 85 }}>
          <ReactMarkdown source={description} />
        </div>
      ) : (
        <InvisibleDescriptionInput
          rows={4}
          value={description}
          onChange={onDescriptionChange}
          placeholder="Description, e.g. Deals an additional **1d4** fire damage on hit"
          disabled={props.disabled}
        />
      )}

      <input type="checkbox" onChange={onPreviewChange} checked={!!isPreview} disabled={props.disabled} />
      <label>Preview</label>
    </BodyWrapper>
  );

  const footer = (
    <React.Fragment>
      <Meta title="Encumbrance">
        <i className="ra ra-kettlebell" />
        <InvisibleNumberInput
          type="number"
          value={encumbrance}
          onChange={onEncumbranceChange}
          min={0}
          placeholder="0"
          disabled={props.disabled}
        />
      </Meta>
      <Meta title="Monetary value">
        <i className="ra ra-gold-bar" />
        <InvisibleNumberInput
          type="number"
          value={value}
          onChange={onValueChange}
          min={0}
          placeholder="0"
          disabled={props.disabled}
        />
      </Meta>
    </React.Fragment>
  );

  return (
    <div>
      <BaseItemCard body={body} header={header} footer={footer} />
      <ButtonContainer>
        <button onClick={onSubmit} disabled={props.disabled}>
          Create
        </button>
      </ButtonContainer>
    </div>
  );
};
