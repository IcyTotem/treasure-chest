import React, { useState, useCallback } from 'react';
import { validNames as validIconNames, RpgIcon } from './RpgIcon';
import Select, { Styles, ValueType } from 'react-select';

const formatOptionLabel = (props: { value: string }) => <RpgIcon name={props.value} />;

const customStyles: Partial<Styles> = {
  option: (provided) => ({
    ...provided,
    color: '#333',
    padding: 5,
    width: 120,
  }),
  control: (provided) => ({
    ...provided,
    width: 120,
    backgroundColor: 'transparent',
    textAlign: 'center',
  }),
  singleValue: () => ({
    color: 'white',
    textAlign: 'center',
  }),
  menu: (provided) => ({
    ...provided,
    width: 120,
  }),
};

export interface Props {
  onChange: (name: string) => void;
  disabled?: boolean;
}

export const RpgIconSelect = (props: Props) => {
  const { onChange } = props;
  const [value, setValue] = useState('');
  const onValueChange = useCallback(
    (e: ValueType<{ value: string; label: string }>) => {
      if (e && 'value' in e) {
        setValue(e.value);
        onChange(e.value);
      }
    },
    [setValue, onChange],
  );

  return (
    <Select
      value={{ value, label: value }}
      options={validIconNames.map((name) => ({ value: name, label: name }))}
      formatOptionLabel={formatOptionLabel}
      styles={customStyles}
      onChange={onValueChange}
      isDisabled={props.disabled}
    />
  );
};
