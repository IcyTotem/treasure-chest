import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Loot, fetchLoot } from '../api/api';
import { ViewItemCard } from './ViewItemCard';
import { useParams } from 'react-router-dom';

const CenteredContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 50px;
`;

export const ViewPage = () => {
  const { id } = useParams();
  const [currentLoot, setCurrentLoot] = useState<Loot>();

  useEffect(() => {
    fetchLoot(id).then(setCurrentLoot);
  }, [id, setCurrentLoot]);

  if (!currentLoot) {
    return <CenteredContainer>Our goblins are fetching your loot... please wait!</CenteredContainer>;
  }

  return (
    <div>
      <CenteredContainer>
        <ViewItemCard {...currentLoot} />
      </CenteredContainer>
      <ButtonContainer>
        <a href={window.location.origin} title="Home page">
          Want to create your own?
        </a>
      </ButtonContainer>
    </div>
  );
};
