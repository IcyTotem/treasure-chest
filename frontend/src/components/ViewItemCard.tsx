import React from 'react';
import styled from 'styled-components';
import ReactMarkdown from 'react-markdown';
import { BaseItemCard } from './BaseItemCard';
import { RpgIcon } from './RpgIcon';

export interface Props {
  name: string;
  description: string;
  icon: string;
  encumbrance?: number;
  value?: number;
}

const Meta = styled.span`
  margin-right: 15px;
  & i.ra {
    margin-right: 5px;
  }
`;

export const ViewItemCard = (props: Props) => (
  <BaseItemCard
    body={<ReactMarkdown source={props.description} />}
    header={
      <React.Fragment>
        <i className={`ra ra-${props.icon}`} />
        <br />
        <span>{props.name}</span>
      </React.Fragment>
    }
    footer={
      (props.encumbrance || props.value) && (
        <React.Fragment>
          {props.encumbrance && (
            <Meta title="Encumbrance">
              <RpgIcon name="kettlebell" />
              <span>{props.encumbrance}</span>
            </Meta>
          )}
          {props.value && (
            <Meta title="Monetary value">
              <RpgIcon name="gold-bar" />
              <span>{props.value}</span>
            </Meta>
          )}
        </React.Fragment>
      )
    }
  />
);
