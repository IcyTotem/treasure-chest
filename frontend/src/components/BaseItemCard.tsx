import React from 'react';
import styled from 'styled-components';

export interface Props {
  header: React.ReactNode;
  body: React.ReactNode;
  footer?: React.ReactNode;
}

const Container = styled.div`
  border: 1px solid rgba(255, 255, 255, 0.2);
  border-radius: 10px;
  padding: 10px;

  @media (min-width: 800px) {
    width: 350px;
  }

  @media (max-width: 799px) {
    width: 200px;
  }
`;

const Header = styled.header`
  padding: 5px;
  border-bottom: 1px solid white;
  font-size: 1.5em;
  text-align: center;

  & i.ra {
    font-size: 1.5em;
  }
`;

const Body = styled.div``;

const Footer = styled.div`
  padding: 5px;
  border-top: 1px solid white;
`;

export const BaseItemCard = (props: Props) => (
  <Container>
    <Header>{props.header}</Header>
    <Body>{props.body}</Body>
    {props.footer && <Footer>{props.footer}</Footer>}
  </Container>
);
