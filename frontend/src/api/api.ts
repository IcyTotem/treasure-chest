const baseUrl = '/api';

export interface LootCreationMeta {
  name: string;
  description: string;
  icon: string;
  encumbrance?: number;
  value?: number;
}

export interface Loot extends LootCreationMeta {
  id: string;
}

const post = (body: unknown) => ({
  method: 'POST',
  body: JSON.stringify(body),
  headers: {
    'Content-Type': 'application/json',
  },
});

const responseBody = async function <T>(response: Response): Promise<T> {
  const json = await response.json();
  if (response.status !== 200) {
    return Promise.reject(json);
  } else {
    return Promise.resolve(json as T);
  }
};

export const createLoot = async (loot: LootCreationMeta): Promise<Loot> => {
  const response = await fetch(`${baseUrl}/loot`, post(loot));
  return responseBody(response);
};

export const fetchLoot = async (id: string): Promise<Loot> => {
  const response = await fetch(`${baseUrl}/loot/${id}`);
  return responseBody(response);
};
