import React from 'react';
import styled from 'styled-components';
import backgroundImage from './assets/background.jpg';
import treasuresImage from './assets/treasures.jpg';
import chestImage from './assets/chest.svg';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { CreationPage } from './components/CreationPage';
import { ViewPage } from './components/ViewPage';

const Body = styled.div<{ backgroundImage: string }>`
  min-height: calc(100% - 40px);
  max-width: 1000px;
  margin: 0 auto;
  padding: 20px;
  border-left: 1px solid rgba(0, 0, 0, 0.5);
  border-right: 1px solid rgba(0, 0, 0, 0.5);
  background-image: url('${(props) => props.backgroundImage}');
  background-size: cover;

  @media (min-width: 800px) {
    width: 80%;
  }
`;

const ShiftingTreasureBackground = styled.img`
  position: fixed;
  top: -100px;
  left: -100px;
  width: 120%;
  height: 120%;
  z-index: -10;

  @media (min-width: 800px) {
    animation: blur-around 40s infinite;
    animation-direction: alternate;
    animation-timing-function: linear;
  }

  @media (max-width: 799px) {
    display: none;
  }
`;

const Title = styled.h1`
  font-size: 2em;
  font-weight: bold;
  text-align: center;
  margin: 0;
`;

const Subtitle = styled.h2`
  font-size: 1em;
  font-weight: normal;
  text-align: center;
  margin: 0;
`;

const MainHeader = styled.header`
  cursor: pointer;
  display: flex;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 40px;

  & img {
    height: 64px;
    margin-left: 20px;
    margin-right: 20px;
  }

  @media (min-width: 800px) {
    flex-direction: row;
  }

  @media (max-width: 799px) {
    flex-direction: column;
    & img:last-child {
      display: none;
    }
  }
`;

const Footer = styled.footer`
  text-align: center;
  font-size: 0.65em;
  padding: 5px;

  @media (min-width: 800px) {
    position: absolute;
    bottom: 0;
  }

  @media (max-width: 799px) {
    margin-top: 80px;
  }
`;

const InnerBody = styled.div`
  width: 100%;
  height: 100%;
`;

const HomePageLink = styled.a`
  color: inherit;
  text-decoration: none;
`;

const App = () => {
  const homePage = window.location.origin;
  return (
    <>
      <ShiftingTreasureBackground src={treasuresImage} />
      <Body backgroundImage={backgroundImage}>
        <InnerBody>
          <HomePageLink href={homePage} title="Treasure chest">
            <MainHeader>
              <img src={chestImage} alt="Treasure chest" />
              <div>
                <Title>Treasure chest</Title>
                <Subtitle>
                  <em>Create and share loot for your games</em>
                </Subtitle>
              </div>
              <img src={chestImage} alt="Treasure chest" />
            </MainHeader>
          </HomePageLink>
          <Router>
            <Switch>
              <Route path="/loot/:id" component={ViewPage} />
              <Route path="/" component={CreationPage} />
            </Switch>
          </Router>
          <Footer>
            <div>
              Chest icon made by&nbsp;
              <a href="https://smashicons.com/" title="Smashicons">
                Smashicons
              </a>
              &nbsp;from&nbsp;
              <a href="https://www.flaticon.com/" title="Flaticon">
                www.flaticon.com
              </a>
              <br />
              Item icons provided by&nbsp;
              <a href="https://nagoshiashumari.github.io/Rpg-Awesome/" title="Rpg Awesome">
                RPG-Awesome
              </a>
            </div>
          </Footer>
        </InnerBody>
      </Body>
    </>
  );
};

export default App;
